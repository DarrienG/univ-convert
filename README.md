# univ-convert - the Apple to universal format converter

For macOS - convert all of your downloaded Apple photo and video files to
universal formats for sharing with your non-apple friends or on social media.

Only works on macOS hosts. Works with zero dependencies other than Python 3.10+
Put it in your `$PATH` and you'll be good to go.

```
🜛 ~ univ-convert --help
usage: univ-convert [-h] file_path [file_path ...]

Autoconvert Apple format photo and video files to standard filetypes

positional arguments:
  file_path   Zip file(s), folder(s) or single file(s) to convert

options:
  -h, --help  show this help message and exit
```

Take a path to a single file, folder, or zip file, and converts to universal
output.

Deletes original file.

* `MOV => mp4`
* `HEIC => jpg`

# Operations run

## Single file

If it's a single `MOV` file, it will convert it to an `mp4` and delete the
original file.

If it's a single `HEIC` file, it will see if it's a live photo and delete its
`MOV` counterpart, then it will convert the `HEIC` file to a `jpg`, and delete
the original on success.

## Folder

Will go through all files in the folder and run the single file operation on it.
It will not convert "live photo videos" to universal format videos, it will just
delete them.

## Zip

Will do the same as folder, except it will unzip the zip file, and delete it
once conversion process is complete.
